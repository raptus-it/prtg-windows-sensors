# Use the following parameters in PRTG:
# Use the the corresponding service principal, which you create with a provided script
# Script Link: https://bitbucket.org/raptus-it/raptus-std-windows-server/src/master/tools/create-azure-ad-service-principal.ps1
#
# -TenantId "<tenantid>" -ApplicationID "<applicationid>" -TenantDomain "<tenantdomain.tld>"
# Install-Module Az

param(
    [parameter(Mandatory=$true)]
    [string]$TenantId,
    [parameter(Mandatory=$true)]
    [String] $ApplicationID,
    [parameter(Mandatory=$true)]
    [String] $TenantDomain
)

Function Get-AzMSSQLBackupInformation {
    Begin {
        $backupVaults = Get-AzRecoveryServicesVault
    }
    Process {
        $dbBackupReport = [System.Collections.ArrayList]::new()
        foreach ($bkupVault in $backupVaults) {
            $recoveryBackupItems = Get-AzRecoveryServicesBackupItem -BackupManagementType AzureWorkload -WorkloadType MSSQL -VaultId $bkupvault.ID
            foreach ($recoveryBackupItem in $recoveryBackupItems) {
                if ($recoveryBackupItem.ProtectionState -eq "Protected") {
                $backedupdbscount++

                } else {
                    $notbackedupdbscount++
                }

                [void]$dbBackupReport.Add([PSCustomObject]@{
                    VM_Name = $recoveryBackupItem.ServerName
                    DB_Name = $recoveryBackupItem.FriendlyName
                    DB_ParentName = $recoveryBackupItem.ParentName
                    DB_ItemHealth = $recoveryBackupItem.ProtectedItemHealthStatus
                    DB_Protected = $recoveryBackupItem.ProtectionStatus
                    DB_LastBackupStatus =  $recoveryBackupItem.LastBackupStatus
                    DB_LastBackupTime = $recoveryBackupItem.LastBackupTime
                    DB_BackupPolicy = $recoveryBackupItem.ProtectionPolicyName
                })
            }
        }
        [void]$dbBackupReport.Add([PSCustomObject]@{
        Total_Backedup_DBs = $backedupdbscount
        Total_Not_Backedup_DBs = $notbackedupdbscount
        })
    }
    End { $dbBackupReport }      
}


$msgtxt = @()
$backedupdbscount = 0
$notbackedupdbscount = 0
$healthybackups = 0
$nothealthybackups = 0
$completedbackups = 0
$notcompletedbackups = 0

$servicename = "prtgmon"
$domain = $TenantDomain
$hostname = Invoke-Command -ScriptBlock {hostname}
$fullname = "$hostname.$servicename-$domain"
$cert = Get-ChildItem -Path Cert:LocalMachine\MY | Where-Object {$_.Subject -like "*$fullname*"}

If (!($cert)) {
    $errortext="No Certificate found. Create Cert and ServicePrincipal first."
		return @"
		<prtg>
		<error>1</error>
		<text>$errortext</text>
		</prtg>
"@
}

Try {
    Connect-AzAccount -ServicePrincipal -Tenant $TenantId -ApplicationId $ApplicationID -CertificateThumbprint $cert.Thumbprint -ErrorAction Stop
} Catch {
    $errortext="$("Connection: " + $_.Exception.Message)"
		return @"
		<prtg>
		<error>1</error>
		<text>$errortext</text>
		</prtg>
"@
}

Try {
    $results = Get-AzMSSQLBackupInformation -ErrorAction Stop
} Catch {
    $errortext="$("Function: " + $_.Exception.Message)"
		return @"
		<prtg>
		<error>1</error>
		<text>$errortext</text>
		</prtg>
"@
}

$totalbackedupdbs = $results.Total_Backedup_DBs
$totalnotbackedupdbs = $results.Total_Not_Backedup_DBs

foreach ($result in $results) {
    If ($result.DB_ItemHealth -eq "Healthy") {
        $healthybackups++
    } elseif ($result.DB_Protected) {
        $nothealthybackups++
        $msgtxt += "Unhealthy: "+$result.DB_Name+" on "+$result.VM_Name+","
    }
    If ($result.DB_LastBackupStatus -eq "Healthy") {
        $completedbackups++
    } elseif ($result.DB_Protected) {
        $notcompletedbackups++
        $msgtxt += "Incomplete: "+$result.DB_Name+" on "+$result.VM_Name+","
    }
}

$msgtxt = $msgtxt | Select-Object -Unique

$XML = "<prtg>"
$XML += "<text>$msgtxt</text>"
$XML += "<result><channel>Total BackedUp DBs</channel><value>$totalbackedupdbs</value><unit>Custom</unit></result>"
$XML += "<result><channel>Total Not BackedUp DBs</channel><value>$totalnotbackedupdbs</value><unit>Custom</unit></result>"
$XML += "<result><channel>Total Healthy Backups</channel><value>$healthybackups</value><unit>Custom</unit></result>"
$XML += "<result><channel>Total Completed Backups</channel><value>$completedbackups</value><unit>Custom</unit></result>"
$XML += "<result><channel>Total Unhealthy Backups</channel><value>$nothealthybackups</value><unit>Custom</unit><LimitMode>1</LimitMode><LimitMaxError>0</LimitMaxError></result>"
$XML += "<result><channel>Total Incomplete Backups</channel><value>$notcompletedbackups</value><unit>Custom</unit><LimitMode>1</LimitMode><LimitMaxError>0</LimitMaxError></result>"
$XML += "</prtg>"

Function WriteXmlToScreen ([xml]$xml) 
{
    $StringWriter = New-Object System.IO.StringWriter;
    $XmlWriter = New-Object System.Xml.XmlTextWriter $StringWriter;
    $XmlWriter.Formatting = "indented";
    $xml.WriteTo($XmlWriter);
    $XmlWriter.Flush();
    $StringWriter.Flush();
    Write-Output $StringWriter.ToString();
}
WriteXmlToScreen "$XML"