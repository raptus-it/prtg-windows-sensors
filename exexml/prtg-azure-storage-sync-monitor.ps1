# Use the following parameters in PRTG:
# I recommend a dedicated Azure AD user with Role "Global reader"
#
# -username "%username" -password "%password"

param(
    [parameter(Mandatory=$true)]
    [string]$username,
    [parameter(Mandatory=$true)]
    [String] $password
)

#PRTG 32bit fix
if ($env:PROCESSOR_ARCHITEW6432 -eq "AMD64") {
    write-warning "Changeing environment to 64bit"
    if ($myInvocation.Line) {
        &"$env:WINDIR\sysnative\windowspowershell\v1.0\powershell.exe" -NonInteractive -NoProfile $myInvocation.Line
    }else{
        &"$env:WINDIR\sysnative\windowspowershell\v1.0\powershell.exe" -NonInteractive -NoProfile -file "$($myInvocation.InvocationName)" $args
    }
exit $lastexitcode
}

# Load need Module MSOnline
$Module = "Az.StorageSync"

# If module is imported say that and do nothing
if (Get-Module | Where-Object {$_.Name -eq $Module}) {
    Write-Host "Module $Module exists"
} 
else {
    # If module is not imported, but available on disk then import
    if (Get-Module -ListAvailable | Where-Object {$_.Name -eq $Module}) {
            Import-Module -Name MSOnline
       }
       else {
              # If module is not imported, not available on disk, but is in online gallery then install and import
            if (Find-Module -Name $m | Where-Object {$_.Name -eq $Module}) {
                Install-Module -Name $Module -Force -Verbose -Scope CurrentUser
                Import-Module -Name $Module
            }
            else {
                # If module is not imported, not available and not in online gallery then abort
                write-host "Module $Module not imported, not available and not in online gallery, exiting."
                EXIT 1
            }
       }
}

#Login to Azure Online
$secureStringPwd = $password | ConvertTo-SecureString -AsPlainText -Force
$credentials = New-Object ('System.Management.Automation.PSCredential') -ArgumentList $username, $secureStringPwd

Try {
    Connect-AzAccount -Credential $credentials
} Catch {
    $errortext="$($_.Exception.Message)"
		return @"
		<prtg>
		<error>1</error>
		<text>$errortext</text>
		</prtg>
"@
}

#Variables
$maxAge = new-timespan -days 1
$healthySyncJobs = 0
$unhealthySyncJobs = 0
$itemSyncErrors = 0
$noSyncSince24h = 0
$msgTxt = ""

Try {
    $ssyncservice = Get-AzStorageSyncService
} Catch {
    $errortext="$($_.Exception.Message)"
        return @"
        <prtg>
        <error>1</error>
        <text>$errortext</text>
        </prtg>
"@  
}

Try {
    $ssyncgroups = get-azstoragesyncgroup -ResourceGroupName $ssyncservice.ResourceGroupName -StorageSyncServiceName $ssyncservice.StorageSyncServiceName
} Catch {
    $errortext="$($_.Exception.Message)"
        return @"
        <prtg>
        <error>1</error>
        <text>$errortext</text>
        </prtg>
"@  
}

foreach ($syncgroup in $ssyncgroups) {
    $ssyncendpoints = Get-AzStorageSyncServerEndpoint -ResourceGroupName $ssyncservice.ResourceGroupName -StorageSyncServiceName $ssyncservice.StorageSyncServiceName -SyncGroupName $syncgroup.SyncGroupName
    
    foreach ($syncendpoint in $ssyncendpoints) {
        #Check for overall Job Health
        if ($syncendpoint.SyncStatus.CombinedHealth -eq "Healthy") {
            $HealthySyncJobs++
        }
        
        if ($syncendpoint.SyncStatus.CombinedHealth -ne "Healthy") {
            $UnhealthySyncJobs++
            $msgtxt += "UNHEALTHY JOB: "+$syncendpoint.SyncGroupName+"("+$syncendpoint.FriendlyName+"), "
        }
        #Check of Specific Item Sync Errors
        if ($syncendpoint.SyncStatus.UploadStatus.LastSyncPerItemErrorCount -gt 0) {
            $ItemSyncErrors++
            $msgtxt += "ITEM SYNC ERROR: "+$syncendpoint.SyncGroupName+"("+$syncendpoint.FriendlyName+"), "
        }

        if ($syncendpoint.SyncStatus.DownloadStatus.LastSyncPerItemErrorCount -gt 0) {
            $ItemSyncErrors++
            $msgtxt += "ITEM SYNC ERROR: "+$syncendpoint.SyncGroupName+"("+$syncendpoint.FriendlyName+"), "
        }
        #Check for Syncs older than 24h
        if (((Get-Date) - $syncendpoint.SyncStatus.UploadStatus.LastSyncTimestamp) -gt $maxAge) {
            $noSyncSince24h++
            $msgtxt += "NO SYNC: "+$syncendpoint.SyncGroupName+"("+$syncendpoint.FriendlyName+"), "
        }

        if (((Get-Date) - $syncendpoint.SyncStatus.DownloadStatus.LastSyncTimestamp) -gt $maxAge) {
            $noSyncSince24h++
            $msgtxt += "NO SYNC: "+$syncendpoint.SyncGroupName+"("+$syncendpoint.FriendlyName+"), "
        }


    }
}

# Results for PRTG Sensor with Channels
$XML = "<prtg>"
$XML += "<text>$msgtxt</text>"
$XML += "<result><channel>HealthySyncJobs</channel><value>$healthySyncJobs</value><unit>Count</unit></result>"
$XML += "<result><channel>UnhealthySyncJobs</channel><value>$unhealthySyncJobs</value><unit>Count</unit><LimitMode>1</LimitMode><LimitMaxError>0</LimitMaxError></result>"
$XML += "<result><channel>ItemSyncErrors</channel><value>$itemSyncErrors</value><unit>Count</unit><LimitMode>1</LimitMode><LimitMaxError>0</LimitMaxError></result>"
$XML += "<result><channel>NoSyncSince24h</channel><value>$noSyncSince24h</value><unit>Count</unit><LimitMode>1</LimitMode><LimitMaxError>0</LimitMaxError></result>"
$XML += "</prtg>"

#Just to make it clean XML code
Function WriteXmlToScreen ([xml]$xml) 
{
    $StringWriter = New-Object System.IO.StringWriter;
    $XmlWriter = New-Object System.Xml.XmlTextWriter $StringWriter;
    $XmlWriter.Formatting = "indented";
    $xml.WriteTo($XmlWriter);
    $XmlWriter.Flush();
    $StringWriter.Flush();
    Write-Output $StringWriter.ToString();
}
WriteXmlToScreen "$XML"