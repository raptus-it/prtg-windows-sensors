param(
[string]$rhost,
[string]$rdomain,
[string]$ruser,
[string]$rpass,
[string]$rscript,
$rscriptargs
)

if( (-Not $rhost) -or (-Not $rdomain) -or (-Not $ruser) -or (-Not $rpass) -or (-Not $rscript) )
{
    Write-Host "ERROR in param"
    exit 1
}

$rspass = $rpass | ConvertTo-SecureString -AsPlainText -force
$cred = new-object -typename System.Management.Automation.PSCredential ( ($rdomain + "\" + $ruser), $rspass)
$ret = Invoke-Command -ComputerName $rhost -Credential $cred -FilePath $rscript -ArgumentList ($rscriptargs -split " ")
$ret
