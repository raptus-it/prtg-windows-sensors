# USAGE
#
# .\prtg-mailstore-monitor.ps1 -mst_user "<username>" -mst_pass "<password>" [-mst_host "<fqdn>"]
#
# Beware: MailStore Administration API must be enabled in MailStore Server Service-Configuration
# Info: Script uses API standard Port 8463
#
Param (
    [Parameter(Mandatory=$false)]
    [ValidateNotNullOrEmpty()]
    [string]$mst_user,

    [Parameter(Mandatory=$false)]
    [ValidateNotNullOrEmpty()]
    [string]$mst_pass,

    [Parameter(Mandatory=$false)]
    [ValidateNotNullOrEmpty()]
    [string]$mst_host="localhost"
)

# -------------------------------------------------------------------

function Raptus-Write-Log {

    [CmdletBinding()]
    Param (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$Message,

        [Parameter(Mandatory=$false)]
        [ValidateSet("Information","Warning","Error")]
        [string]$LogLevel="Information",

        [Parameter(Mandatory=$false)]
        [int]$EventID=27002,

        [Parameter(Mandatory=$false)]
        [string]$Source = (Get-Item $MyInvocation.ScriptName).Name
    )
    Begin {
        $VerbosePreference = 'Continue'
        $startmsg = Get-EventLog -list | Where-Object {$_.logdisplayname -eq "Raptus"}
        if (-Not $startmsg) {
            New-EventLog -LogName Raptus -Source "Raptus" -ErrorAction SilentlyContinue
            Write-EventLog -LogName Raptus -Source "Raptus" -EntryType Information -EventId 27001 -Message "Raptus Log Start"
        }
        $checksource = Get-EventLog -LogName Raptus | Where-Object {$_.source -eq "$Source"}
        if (-Not $checksource) {
            New-EventLog -LogName Raptus -Source $Source
        }
    }
    Process {
        Write-EventLog -LogName Raptus -Message $Message -EntryType $LogLevel -EventId $EventID -Source $Source
    }
    End { }
}

# -------------------------------------------------------------------

$PRTG_XML_OK = 0                   # OK
$PRTG_XML_WARNING = 1              # Warning
$PRTG_XML_ERROR_SYSTEM = 2         # System Error (e.g. a network/socket error)
$PRTG_XML_ERROR_PROTOCOL = 3       # Protocol Error (e.g. web server returns a 404)
$PRTG_XML_ERROR_CONTENT = 4        # Content Error (e.g. a web page does not contain a required word)

function PRTG-XML-ReturnError {
    Param( [int] $prtg_id,
           [string] $message,
           [boolean] $log_it = $TRUE)

    if( $log_it ) {
        Raptus-Write-Log -Message $message -Source "PRTG Mailstore Monitor"
    }

    Write-Host "<prtg>"
    Write-Host "<error>$prtg_id</error>"
    Write-Host "<text>$message</text>"
    Write-Host "</prtg>"
    exit
}

# -------------------------------------------------------------------

$fromInc = (Get-Date).AddDays(-1) | Get-Date -Format "yyyy-MM-ddTHH:mm:ss"
$toExc = Get-Date -Format "yyyy-MM-ddTHH:mm:ss"
$msgtxt = @()
$cycle_count = 0
$cycle_count_failed = 0
$job_count = 0
$job_count_failed = 0

Try {
    Import-Module "${env:ProgramFiles(x86)}\PRTG Network Monitor\Custom Sensors\EXEXML\prtg-mailstore-monitor\MS.PS.Lib.psd1"
    $msapiclient = New-MSApiClient -Username $mst_user -Password $mst_pass -Server $mst_host -Port 8463 -IgnoreInvalidSSLCerts
    $workers = Invoke-MSApiCall $msapiclient "GetWorkerResults" @{fromIncluding = $fromInc; toExcluding = $toExc; timeZoneID ="W. Europe Standard Time"}
} Catch {
    PRTG-XML-ReturnError -prtg_id $PRTG_XML_ERROR_SYSTEM -message $_.Exception.Message
}

foreach ($worker in $workers.result) {
    $cycle_count++
    if (-Not($worker.result -eq "succeeded")) {
        $cycle_count_failed++
        $msgtxt += $worker.profileName+", "
    }
}

foreach ($job in $jobs.result) {
    $job_count++
    if (-Not($job.result -eq "succeeded")) {
        $job_count_failed++
        $msgtxt += $job.profileName+", "
    }
}

$msgtxt = $msgtxt | Select-Object -Unique

Write-Host "<prtg>"
Write-Host "<text>$msgtxt</text>"
Write-Host "<result><channel>Worker Cycles (last 24h)</channel><value>$cycle_count</value><unit>Count</unit></result>"
Write-Host "<result><channel>Failed Cycles (last 24h)</channel><value>$cycle_count_failed</value><unit>Count</unit><LimitMode>1</LimitMode><LimitMaxError>0</LimitMaxError></result>"
Write-Host "</prtg>"