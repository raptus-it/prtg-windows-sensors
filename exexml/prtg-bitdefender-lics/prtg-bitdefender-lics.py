#!/opt/local/bin/python
# -*- coding: utf-8 -*-

import os
import io
import sys
import base64
import hashlib
import argparse
import time
import datetime
import json
import re
import unicodedata
import urllib
import requests
import simplejson
import uuid

from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

ENCODING = "utf-8"

GZONE_API_URL = "https://cloudgz.gravityzone.bitdefender.com/api/v1.0/jsonrpc/licensing"
GZONE_API_CRED_FILE = "auth/gzone-api-auth.json"

#####################################################################
# Main
#####################################################################

with open(GZONE_API_CRED_FILE, "rb") as gzone_auth_file:
    gzone_auth = json.load(gzone_auth_file)

encodedUserPassSequence = base64.b64encode((gzone_auth["gzone_api_key"]+":").encode(ENCODING))
authorizationHeader = "Basic ".encode(ENCODING) + encodedUserPassSequence
http_headers = { "Content-Type": "application/json", "Authorization": authorizationHeader }
json_request = json.dumps( {
    "id": str(uuid.uuid4()),
    "jsonrpc": "2.0",
    "method": "getLicenseInfo",
    "params": {
    }
} )

result = requests.post(GZONE_API_URL, json_request, verify=False, headers=http_headers)
jsonResult = simplejson.loads(result.content)

# EXAMPLE RETURN in jsonResult
#
# {
#   "id": "6826c765-d925-48c4-9cea-c37351dd5b06",
#   "jsonrpc": "2.0",
#   "result": {
#     "subscriptionType": 3,
#     "expiryDate": "2030-02-18T19:04:00",
#     "usedSlots": 822,
#     "reservedSlots": 0,
#     "totalSlots": 850,
#     "manageExchange": true,
#     "manageEncryption": true,
#     "manageRemoteEnginesScanning": true,
#     "manageHyperDetect": false,
#     "manageSandboxAnalyzer": false,
#     "managePatchManagement": true,
#     "manageEventCorrelator": false,
#     "manageEmailSecurity": false,
#     "assignedProductType": 0,
#     "additionalProductTypes": [
#       0
#     ]
#   }
# }

print("<prtg>")
print("<result><channel>Available Slots</channel><value>" + str(jsonResult["result"]["totalSlots"] - jsonResult["result"]["usedSlots"]) + "</value><unit>Count</unit></result>")
print("<result><channel>Total Slots</channel><value>" + str(jsonResult["result"]["totalSlots"]) + "</value><unit>Count</unit></result>")
print("<result><channel>Used Slots</channel><value>" + str(jsonResult["result"]["usedSlots"]) + "</value><unit>Count</unit></result>")
print("</prtg>")

# eof

