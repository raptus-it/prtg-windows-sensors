# Use the following parameters in PRTG:
# I recommend a dedicated Azure AD user with Role "Directory readers"
#
# -username "%username" -password "%password"

param(
    [parameter(Mandatory=$true)]
    [string]$username,
    [parameter(Mandatory=$true)]
    [String] $password
)


#PRTG 32bit fix
if ($env:PROCESSOR_ARCHITEW6432 -eq "AMD64") {
    write-warning "Changeing environment to 64bit"
    if ($myInvocation.Line) {
        &"$env:WINDIR\sysnative\windowspowershell\v1.0\powershell.exe" -NonInteractive -NoProfile $myInvocation.Line
    }else{
        &"$env:WINDIR\sysnative\windowspowershell\v1.0\powershell.exe" -NonInteractive -NoProfile -file "$($myInvocation.InvocationName)" $args
    }
exit $lastexitcode
}

# Load need Module MSOnline
$Module = "MSOnline"

# If module is imported say that and do nothing
if (Get-Module | Where-Object {$_.Name -eq $Module}) {
    Write-Host "Module $Module exists"
} 
else {
    # If module is not imported, but available on disk then import
    if (Get-Module -ListAvailable | Where-Object {$_.Name -eq $Module}) {
            Import-Module -Name MSOnline
       }
       else {
              # If module is not imported, not available on disk, but is in online gallery then install and import
            if (Find-Module -Name $m | Where-Object {$_.Name -eq $Module}) {
                Install-Module -Name $Module -Force -Verbose -Scope CurrentUser
                Import-Module -Name $Module
            }
            else {
                # If module is not imported, not available and not in online gallery then abort
                write-host "Module $Module not imported, not available and not in online gallery, exiting."
                EXIT 1
            }
       }
}

#Login to Microsoft Online
$secureStringPwd = $password | ConvertTo-SecureString -AsPlainText -Force
$credentials = New-Object ('System.Management.Automation.PSCredential') -ArgumentList $username, $secureStringPwd
try{
    Connect-MsolService -Credential $credentials -Verbose
}
catch {
$errortext="$($_.Exception.Message)"
		return @"
		<prtg>
		<error>1</error>
		<text>$errortext</text>
		</prtg>
"@
}

#Variables
$DirSyncStatus = 0
$PWSyncStatus = 0
$MSOLService = Get-MsolCompanyInformation
$msgtxt = ""

#Check if User has any sync error
$usersyncstates = Get-MSOlUser -ALL | Select-Object UserPrincipalName, DirSyncProvisioningErrors
$usersyncerror = @()
foreach ($user in $usersyncstates) {
    if ($user.DirSyncProvisioningErrors){
        $usersyncerror += $user.UserPrincipalName
    }
}
$usersyncerrorcount = $usersyncerror.count
$msgtxt = $usersyncerror

#Check last Sync Time | Variable in PRTG $DirSync
$LastDirSync = $MSOLService.LastDirSyncTime
$LocalTimeinUTC = (get-date).ToUniversalTime()
$DirSync = ($LocalTimeinUTC - $LastDirSync).TotalMinutes

#Check last Password Sync Time | Variable in PRTG $PWSync
$LastPWSync = $MSOLService.LastPasswordSyncTime
$LocalTimeinUTC = (get-date).ToUniversalTime()
$PWSync = ($LocalTimeinUTC - $LastPWSync).TotalMinutes

#Check Password Sync Status | Variable in PRTG $PasswordSyncEnabled
$PasswordSyncEnabled = $MSOLService.PasswordSynchronizationEnabled
if ( $PasswordSyncEnabled -eq "True" ) {
    $PasswordSync = 1 
} else {
    $PasswordSync = 0
}

#Check Synced User | Variable in PRTG $SyncedUser
$SyncedUser = (Get-MsolUser -ALL -Synchronized | Measure-Object).Count

#Check Synced User | Variable in PRTG $SyncedGroups
$SyncedGroups = (Get-MsolGroup).count

#Get the Service Principal for EWERK Monitoring
$SP_PRTG_Monitoring_Password = (Get-MsolUser -UserPrincipalName $username).LastPasswordChangeTimestamp.AddDays(90)
$SP_PRTG_Monitoring_Days_left = ($SP_PRTG_Monitoring_Password - $LocalTimeinUTC).Days

# Results for PRTG Sensor with Channels
$XML = "<prtg>"
$XML += "<text>$msgtxt</text>"
$XML += "<result><channel>SyncErrors</channel><value>$usersyncerrorcount</value><unit>Count</unit><LimitMode>1</LimitMode><LimitMaxError>0</LimitMaxError></result>"
$XML += "<result><channel>PasswordSyncStatus</channel><value>$PasswordSync</value></result>"
$XML += "<result><channel>LastDirSync</channel><value>$DirSync</value><float>1</float><DecimalMode>Auto</DecimalMode><LimitMode>1</LimitMode><LimitMaxError>75</LimitMaxError></result>"
$XML += "<result><channel>LastPWSync</channel><value>$PWSync</value><float>1</float><DecimalMode>Auto</DecimalMode><LimitMode>1</LimitMode><LimitMaxError>75</LimitMaxError></result>"
$XML += "<result><channel>SyncedUser</channel><value>$SyncedUser</value><unit>Count</unit></result>"
$XML += "<result><channel>SyncedGroup</channel><value>$SyncedGroups</value><unit>Count</unit></result>"
$XML += "<result><channel>ServicePrincipalPasswordTimer</channel><value>$SP_PRTG_Monitoring_Days_left</value></result>"
$XML += "</prtg>"

#Just to make it clean XML code
Function WriteXmlToScreen ([xml]$xml) 
{
    $StringWriter = New-Object System.IO.StringWriter;
    $XmlWriter = New-Object System.Xml.XmlTextWriter $StringWriter;
    $XmlWriter.Formatting = "indented";
    $xml.WriteTo($XmlWriter);
    $XmlWriter.Flush();
    $StringWriter.Flush();
    Write-Output $StringWriter.ToString();
}
WriteXmlToScreen "$XML"