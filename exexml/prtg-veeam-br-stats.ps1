Param(
[Parameter(Mandatory=$true)] [string] $rhost,
[Parameter(Mandatory=$true)] [string] $rdomain,
[Parameter(Mandatory=$true)] [string] $ruser,
[Parameter(Mandatory=$true)] [string] $rpass,
[Parameter(Mandatory=$false)] [string] $selChann = "BCRE"
)

# Use the following parameters in PRTG
#
# -rhost "%host" -rdomain "%windowsdomain" -ruser "%windowsuser" -rpass "%windowspassword" -selChann "BCRE"

$rexec = "${env:ProgramFiles(x86)}\PRTG Network Monitor\Custom Sensors\EXEXML\prtg-veeam-br-stats\remote-exec-ps.ps1"
$rscript = "${env:ProgramFiles(x86)}\PRTG Network Monitor\Custom Sensors\EXEXML\prtg-veeam-br-stats\get-veeam-br-stats.ps1"
$brargs = @("localhost",24,5,10,$selChann)

C:\Windows\sysnative\windowspowershell\v1.0\powershell.exe -File $rexec -rhost $rhost -rdomain $rdomain -ruser $ruser -rpass $rpass -rscript $rscript -rscriptargs (,$brargs)
