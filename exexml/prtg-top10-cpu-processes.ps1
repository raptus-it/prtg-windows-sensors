$topTenProcs = Get-Process -IncludeUserName | Sort-Object CPU -desc | Select-Object -first 10

Write-Host "<prtg>"
foreach( $topProc in $topTenProcs ) {
    Write-Host "<result><channel>$($topProc.Name) ($($topProc.UserName -replace '.*\\'))</channel><value>$($topProc.TotalProcessorTime.Hours)</value><unit>TimeHours</unit></result>"
}
Write-Host "</prtg>" 

