# Use the following parameters in PRTG:
# Use the the corresponding service principal, which you create with a provided script
# The "HostnameOverwrite" parameter is only for certificates, that have been moved from the old monitoring server (moshene)
# Script Link: https://bitbucket.org/raptus-it/raptus-std-windows-server/src/master/tools/create-azure-ad-service-principal.ps1
#
# -TenantId "<tenantid>" -ApplicationID "<applicationid>" -TenantDomain "<tenantdomain.tld>" [-HostnameOverwrite "<hostname>"]
# Install-Module Az

param(
    [parameter(Mandatory=$true)]
    [string]$TenantId,
    [parameter(Mandatory=$true)]
    [String] $ApplicationID,
    [parameter(Mandatory=$true)]
    [String] $TenantDomain,
    [parameter(Mandatory=$false)]
    [String] $HostnameOverwrite
)

Function Get-AzVMBackupInformation {
    Begin {
        $vms = Get-AzVM
        $backupVaults = Get-AzRecoveryServicesVault
    }
    Process {
        $vmBackupReport = [System.Collections.ArrayList]::new()
        foreach ($vm in $vms) {
            $recoveryVaultInfo = Get-AzRecoveryServicesBackupStatus -Name $vm.Name -ResourceGroupName $vm.ResourceGroupName -Type 'AzureVM'
            if ($recoveryVaultInfo.BackedUp -eq $true) {
                $backedupvmscount++
                #Backup Recovery Vault Information
                $vmBackupVault = $backupVaults | Where-Object {$_.ID -eq $recoveryVaultInfo.VaultId}

                #Backup recovery Vault policy Information
                $container = Get-AzRecoveryServicesBackupContainer -ContainerType AzureVM -VaultId $vmBackupVault.ID -FriendlyName $vm.Name #-Status "Registered"
                $backupItem = Get-AzRecoveryServicesBackupItem -Container $container -WorkloadType AzureVM -VaultId $vmBackupVault.ID
            } else {
                $notbackedupvmscount++
                $vmBackupVault = $null
                $container =  $null
                $backupItem =  $null
            }
        
            [void]$vmBackupReport.Add([PSCustomObject]@{
                VM_Name = $vm.Name
                VM_Location = $vm.Location
                VM_ResourceGroupName = $vm.ResourceGroupName
                VM_BackedUp = $recoveryVaultInfo.BackedUp
                VM_RecoveryVaultName =  $vmBackupVault.Name
                VM_RecoveryVaultPolicy = $backupItem.ProtectionPolicyName
                VM_BackupHealthStatus = $backupItem.HealthStatus
                VM_BackupProtectionStatus = $backupItem.ProtectionStatus
                VM_LastBackupStatus = $backupItem.LastBackupStatus
                VM_LastBackupTime = $backupItem.LastBackupTime
                VM_BackupDeleteState = $backupItem.DeleteState
                VM_BackupLatestRecoveryPoint = $backupItem.LatestRecoveryPoint
                VM_Id = $vm.Id
                RecoveryVault_ResourceGroupName = $vmBackupVault.ResourceGroupName
                RecoveryVault_Location = $vmBackupVault.Location
                RecoveryVault_SubscriptionId = $vmBackupVault.ID
            })
        } #foreach ($vm in $vms)
        [void]$vmBackupReport.Add([PSCustomObject]@{
            Total_Backedup_VMs = $backedupvmscount
            Total_Not_Backedup_VMs = $notbackedupvmscount
        })
    }
    End { $vmBackupReport }
}

$msgtxt = @()
$backedupvmscount = 0
$notbackedupvmscount = 0
$healthybackups = 0
$nothealthybackups = 0
$completedbackups = 0
$notcompletedbackups = 0

$servicename = "prtgmon"
$domain = $TenantDomain

if ($HostnameOverwrite) {
    $hostname = $HostnameOverwrite 
} else {
    $hostname = Invoke-Command -ScriptBlock {hostname}
}

$fullname = "$hostname.$servicename-$domain"
$cert = Get-ChildItem -Path Cert:LocalMachine\MY | Where-Object {$_.Subject -like "*$fullname*"}

If (!($cert)) {
    $errortext="No Certificate found. Create Cert and ServicePrincipal first."
		return @"
		<prtg>
		<error>1</error>
		<text>$errortext</text>
		</prtg>
"@
}

Try {
    Connect-AzAccount -ServicePrincipal -Tenant $TenantId -ApplicationId $ApplicationID -CertificateThumbprint $cert.Thumbprint -ErrorAction Stop
} Catch {
    $errortext="$("Connection: " + $_.Exception.Message)"
		return @"
		<prtg>
		<error>1</error>
		<text>$errortext</text>
		</prtg>
"@
}

Try {
    $results = Get-AzVMBackupInformation -ErrorAction Stop
} Catch {
    $errortext="$("Function: " + $_.Exception.Message)"
		return @"
		<prtg>
		<error>1</error>
		<text>$errortext</text>
		</prtg>
"@
}

$totalbackedupvms = $results.Total_BackedUp_VMs
$totalnotbackedupvms = $results.Total_Not_BackedUp_VMs

foreach ($result in $results) {
    If ($result.VM_BackupProtectionStatus -eq "Healthy") {
        $healthybackups++
    } elseif ($result.VM_BackedUp) {
        $nothealthybackups++
        $msgtxt += "Unhealthy: "+$result.VM_Name+","
    }
    If ($result.VM_LastBackupStatus -eq "Completed") {
        $completedbackups++
    } elseif ($result.VM_BackedUp) {
        $notcompletedbackups++
        $msgtxt += "Incomplete: "+$result.VM_Name+","
    }
}

$msgtxt = $msgtxt | Select-Object -Unique

$XML = "<prtg>"
$XML += "<text>$msgtxt</text>"
$XML += "<result><channel>Total BackedUp VMs</channel><value>$totalbackedupvms</value><unit>Custom</unit></result>"
$XML += "<result><channel>Total Not BackedUp VMs</channel><value>$totalnotbackedupvms</value><unit>Custom</unit></result>"
$XML += "<result><channel>Total Healthy Backups</channel><value>$healthybackups</value><unit>Custom</unit></result>"
$XML += "<result><channel>Total Completed Backups</channel><value>$completedbackups</value><unit>Custom</unit></result>"
$XML += "<result><channel>Total Unhealthy Backups</channel><value>$nothealthybackups</value><unit>Custom</unit><LimitMode>1</LimitMode><LimitMaxError>0</LimitMaxError></result>"
$XML += "<result><channel>Total Incomplete Backups</channel><value>$notcompletedbackups</value><unit>Custom</unit><LimitMode>1</LimitMode><LimitMaxError>0</LimitMaxError></result>"
$XML += "</prtg>"

#Just to make it clean XML code
Function WriteXmlToScreen ([xml]$xml) 
{
    $StringWriter = New-Object System.IO.StringWriter;
    $XmlWriter = New-Object System.Xml.XmlTextWriter $StringWriter;
    $XmlWriter.Formatting = "indented";
    $xml.WriteTo($XmlWriter);
    $XmlWriter.Flush();
    $StringWriter.Flush();
    Write-Output $StringWriter.ToString();
}
WriteXmlToScreen "$XML"