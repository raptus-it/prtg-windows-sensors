# Use the following parameters in PRTG:
# I recommend a dedicated Azure AD user with Role "Directory readers"
#
# -url "%url" -username "%username" -password "%password"
# example url: "https://veeamcloud.notzgroup.net

param(
    [parameter(Mandatory=$true)]
    [string]$url,
    [parameter(Mandatory=$true)]
    [String] $username,
    [parameter(Mandatory=$true)]
    [String] $password
)

#var
$msgtxt =""

function Get-VeeamApiToken {
    $uri = "$url/api/oauth2/token"
    $body = @{
        "grant_type" = "Password"
        "Username" = $username
        "Password" = $password
    }
$access = Invoke-RestMethod -Method 'Post' -Uri $uri -Body $body
$access.access_token
}

function Get-VeeamJobSessions {
    $token = Get-VeeamApiToken
    $uri = "$url/api/v2/jobSessions"
    $header = @{
    Authorization="Bearer $token"
    }
    $data = Invoke-RestMethod -Method Get -Uri $uri -Headers $header -UseBasicParsing -ContentType "application/json" 
    $data.results
}

$oneday = (Get-Date).AddDays(-1) | Get-Date -Format "yyyy-MM-ddTHH:mm:ss"
$msgtxt = @()
$job_count = 0
$job_count_success = 0
$job_count_warning = 0
$job_count_failed = 0
$job_count_running = 0
$job_count_canceled = 0

#Build Object Array
Try {
    $results = Get-VeeamJobSessions `
                | Select-Object -Property type, `
                @{Name="executionStartTime"; Expression={$_.executionStartTime | Get-Date -Format "yyyy-MM-ddTHH:mm:ss"}}, `
                @{Name="executionDuration"; Expression={$_.executionDuration | Get-Date -Format "HH:mm:ss"}}, `
                status,backupJobInfo `
                | Where-Object {$_.executionStartTime -ge $oneday}
}
Catch {
$errortext="$($_.Exception.Message)"
		return @"
		<prtg>
		<error>1</error>
		<text>$errortext</text>
		</prtg>
"@
}


foreach ($result in $results) {
    $job_count++
    if ($result.status -eq "Success") {
        $job_count_success++
    } elseif ($result.status -eq "Warning") {
        $job_count_warning++
        $msgtxt += "Warning: "+$result.backupJobInfo.policyName+","
    } elseif ($result.status -eq "Error") {
        $job_count_failed++
        $msgtxt += "Failed: "+$result.backupJobInfo.policyName+","
    } elseif ($result.status -eq "Running") {
        $job_count_running++
    } elseif ($result.status -eq "Canceled") {
        $job_count_canceled++
    }
}

$msgtxt = $msgtxt | Select-Object -Unique

# Results for PRTG Sensor with Channels
$XML = "<prtg>"
$XML += "<text>$msgtxt</text>"
$XML += "<result><channel>Total Jobs (24h)</channel><value>$job_count</value><unit>Custom</unit></result>"
$XML += "<result><channel>Jobs Successful (24h)</channel><value>$job_count_success</value><unit>Custom</unit></result>"
$XML += "<result><channel>Jobs Warning (24h)</channel><value>$job_count_warning</value><unit>Custom</unit><LimitMode>1</LimitMode><LimitMaxWarning>0</LimitMaxWarning></result>"
$XML += "<result><channel>Jobs Failed (24h)</channel><value>$job_count_failed</value><unit>Custom</unit><LimitMode>1</LimitMode><LimitMaxError>0</LimitMaxError></result>"
$XML += "<result><channel>Jobs Running (24h)</channel><value>$job_count_running</value><unit>Custom</unit></result>"
$XML += "<result><channel>Jobs Cancelled (24h)</channel><value>$job_count_canceled</value><unit>Custom</unit></result>"
$XML += "</prtg>"

#Just to make it clean XML code
Function WriteXmlToScreen ([xml]$xml) 
{
    $StringWriter = New-Object System.IO.StringWriter;
    $XmlWriter = New-Object System.Xml.XmlTextWriter $StringWriter;
    $XmlWriter.Formatting = "indented";
    $xml.WriteTo($XmlWriter);
    $XmlWriter.Flush();
    $StringWriter.Flush();
    Write-Output $StringWriter.ToString();
}
WriteXmlToScreen "$XML"