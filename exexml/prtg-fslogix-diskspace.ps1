Param( [string] $connection_broker_host )
$ErrorActionPreference = "Continue"

#PRTG 32bit fix
if ($env:PROCESSOR_ARCHITEW6432 -eq "AMD64") {
    write-warning "Changeing environment to 64bit"
    if ($myInvocation.Line) {
        &"$env:WINDIR\sysnative\windowspowershell\v1.0\powershell.exe" -NonInteractive -NoProfile $myInvocation.Line
    }else{
        &"$env:WINDIR\sysnative\windowspowershell\v1.0\powershell.exe" -NonInteractive -NoProfile -file "$($myInvocation.InvocationName)" $args
    }
exit $lastexitcode
}

# -------------------------------------------------------------------

$MB_WARN = 1536
$MB_ERR = 1024

# -------------------------------------------------------------------

$WINVER_OTHER = 0
$WINVER_08R2 = 1
$WINVER_12R2 = 2
$WINVER_16 = 3

function Detect-Windows-Server-Version {
    $winv = (Get-WmiObject -class Win32_OperatingSystem).Caption
    switch -regex ($winv) {
        ".*2008 R2.*"   { return $WINVER_08R2 }
        ".*2012 R2.*"   { return $WINVER_12R2 }
        ".*2016.*"      { return $WINVER_16 }
        default         { return $WINVER_OTHER }
    }
}

$winver = Detect-Windows-Server-Version

# -------------------------------------------------------------------

function Raptus-Write-Log {

    [CmdletBinding()]
    Param (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$Message,

        [Parameter(Mandatory=$false)]
        [ValidateSet("Information","Warning","Error")]
        [string]$LogLevel="Information",

        [Parameter(Mandatory=$false)]
        [int]$EventID=27002,

        [Parameter(Mandatory=$false)]
        [string]$Source = (Get-Item $MyInvocation.ScriptName).Name
    )
    Begin {
        $VerbosePreference = 'Continue'
        $startmsg = Get-EventLog -list | Where-Object {$_.logdisplayname -eq "Raptus"}
        if (-Not $startmsg) {
            New-EventLog -LogName Raptus -Source "Raptus" -ErrorAction SilentlyContinue
            Write-EventLog -LogName Raptus -Source "Raptus" -EntryType Information -EventId 27001 -Message "Raptus Log Start"
        }
        $checksource = Get-EventLog -LogName Raptus | Where-Object {$_.source -eq "$Source"}
        if (-Not $checksource) {
            New-EventLog -LogName Raptus -Source $Source
        }
    }
    Process {
        Write-EventLog -LogName Raptus -Message $Message -EntryType $LogLevel -EventId $EventID -Source $Source
    }
    End { }
}

# -------------------------------------------------------------------

$PRTG_XML_OK = 0                   # OK
$PRTG_XML_WARNING = 1              # Warning
$PRTG_XML_ERROR_SYSTEM = 2         # System Error (e.g. a network/socket error)
$PRTG_XML_ERROR_PROTOCOL = 3       # Protocol Error (e.g. web server returns a 404)
$PRTG_XML_ERROR_CONTENT = 4        # Content Error (e.g. a web page does not contain a required word)

function PRTG-XML-ReturnError {
    Param( [int] $prtg_id,
           [string] $message,
           [boolean] $log_it = $TRUE)

    if( $log_it ) {
        Raptus-Write-Log -Message $message -Source "RDS-UPD-Sensor"
    }

    Write-Host "<prtg>"
    Write-Host "<error>$prtg_id</error>"
    Write-Host "<text>$message</text>"
    Write-Host "</prtg>"
    exit
}

# -------------------------------------------------------------------

function Detect-Connection-Broker {

    Try {
       $q = Get-RDServer -ErrorAction Stop | Where { $_.Roles -eq "RDS-CONNECTION-BROKER" }
       $cbh = $q.Server
       $cbh = $cbh.ToLower()
    }
    Catch  {
        throw $_.Exception.Message
    }

    if( -Not (Test-Connection -ComputerName $cbh -Quiet) ) {
        throw "Connection broker host $connection_broker_host is not reachable"
    }

    return $cbh
}

#
# Optional parameter: Override the detection of connection broker host
#
function RDS-Session-Host-List {
    Param( [string] $cbh )

    if( -Not ($cbh) ) {
        throw "Connection broker not specified"
    }
    
    Try {
        $rdshs = Get-RDServer -ConnectionBroker $cbh -ErrorAction Stop | Where { $_.Roles -eq "RDS-RD-SERVER" } | Select Server
    }
    Catch {
        throw $_.Exception.Message
    }

    return $rdshs
}

# -------------------------------------------------------------------

$fslodfc_count = 0
$fslprof_count = 0
$upd_count_warn = 0
$upd_count_err = 0
$upd_list_warn = ""
$upd_list_err = ""

Try {
    if( ($winver -eq $WINVER_16) -or ($winver -eq $WINVER_12R2) ) {

        Import-Module RemoteDesktop

        if( -Not ($connection_broker_host) ) {
            $connection_broker_host = Detect-Connection-Broker
        }

        $rdsh_list = RDS-Session-Host-List $connection_broker_host
        if( -Not ( $rdsh_list ) ) {
            PRTG-XML-ReturnError -prtg_id $PRTG_XML_ERROR_SYSTEM -message "No RDS session hosts found"
        }

        foreach( $rdsh in $rdsh_list) {
            $mounts = Get-WmiObject -ComputerName $rdsh.Server -Class Win32_Volume -Filter {SystemVolume = "False" and DriveLetter = NULL and FileSystem != "CDFS"}
            foreach( $mnt in $mounts) {

                if (-Not($mnt.DriveLetter)) {
                    if ($mnt.Label -like "O365-*") { 
                        $fslodfc_count++
                    } else {
                        $fslprof_count++
                    }
                    $freeMB = [math]::Round($mnt.Freespace/1MB)

                    if( $freeMB -le $MB_WARN -and $freeMB -gt $MB_ERR ) {
                        $upd_count_warn++
                        $upd_list_warn += $mnt.Label+" "
                    }
                    elseif( $freeMB -le $MB_ERR ) {
                        $upd_count_err++
                        $upd_list_err += $mnt.Label+" "
                        $message = "Less than $MB_ERR MB free space in Container $($mnt.Label) mounted on $($mnt.PSComputerName)"
                        Raptus-Write-Log -Message $message -Source "RDS-UPD-Sensor"
                    }
                }
            }
        }
    }
    else {
        PRTG-XML-ReturnError -prtg_id $PRTG_XML_ERROR_SYSTEM -message  "Unsupported Windows Server Version"
    }
}
Catch  {
    PRTG-XML-ReturnError -prtg_id $PRTG_XML_ERROR_SYSTEM -message $_.Exception.Message
}

if ($upd_list_warn) {
    $msgtxt += "WARN: $upd_list_warn "
}

if ($upd_list_err) {
    $msgtxt += "ERR: $upd_list_err "
}

Write-Host "<prtg>"
Write-Host "<text>$msgtxt</text>"
Write-Host "<result><channel>Mounted Profile Container</channel><value>$fslprof_count</value><unit>Count</unit></result>"
Write-Host "<result><channel>Mounted Office 365 Container</channel><value>$fslodfc_count</value><unit>Count</unit></result>"
Write-Host "<result><channel>Container Space lower $MB_WARN MB</channel><value>$upd_count_warn</value><unit>Count</unit><LimitMode>1</LimitMode><LimitMaxWarning>0</LimitMaxWarning></result>"
Write-Host "<result><channel>Container Space lower $MB_ERR MB</channel><value>$upd_count_err</value><unit>Count</unit><LimitMode>1</LimitMode><LimitMaxError>0</LimitMaxError></result>"
Write-Host "</prtg>"

# eof