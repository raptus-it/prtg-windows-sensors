# Use the following parameters in PRTG:
# Use the the corresponding service principal, which you create with a provided script
# The "HostnameOverwrite" parameter is only for certificates, that have been moved from the old monitoring server (moshene)
# Script Link: https://bitbucket.org/raptus-it/raptus-std-windows-server/src/master/tools/create-azure-ad-service-principal.ps1
#
# -TenantId "<tenantid>" -ApplicationID "<applicationid>" -TenantDomain "<tenantdomain.tld>" [-HostnameOverwrite "<hostname>"]
# Install-Module Az

param(
    [parameter(Mandatory=$true)]
    [string]$TenantId,
    [parameter(Mandatory=$true)]
    [String] $ApplicationID,
    [parameter(Mandatory=$true)]
    [String] $TenantDomain,
    [parameter(Mandatory=$false)]
    [String] $HostnameOverwrite
)

Function Get-AzFileshareBackupInformation {
    Begin {
        $backupVaults = Get-AzRecoveryServicesVault
    }
    Process {
        $fileBackupReport = [System.Collections.ArrayList]::new()
        foreach ($bkupVault in $backupVaults) {
            $recoveryBackupItems = Get-AzRecoveryServicesBackupItem -BackupManagementType AzureStorage -WorkloadType AzureFiles -VaultId $bkupvault.ID
            foreach ($recoveryBackupItem in $recoveryBackupItems) {
                if ($recoveryBackupItem.ProtectionState -eq "Protected") {
                $backedupfilesharescount++

                } else {
                    $notbackedupfilesharescount++
                }

                [void]$fileBackupReport.Add([PSCustomObject]@{
                    FS_Name = $recoveryBackupItem.FriendlyName
                    FS_Protected = $recoveryBackupItem.ProtectionStatus
                    FS_ProtectionState = $recoveryBackupItem.ProtectionState
                    FS_LastBackupStatus =  $recoveryBackupItem.LastBackupStatus
                    FS_LastBackupTime = $recoveryBackupItem.LastBackupTime
                    FS_BackupPolicy = $recoveryBackupItem.ProtectionPolicyName
                })
            }
        }
        [void]$fileBackupReport.Add([PSCustomObject]@{
        Total_Backedup_Fileshares = $backedupfilesharescount
        Total_Not_Backedup_Fileshares = $notbackedupfilesharescount
        })
    }
    End { $fileBackupReport }      
}


$msgtxt = @()
$backedupfilesharescount = 0
$notbackedupfilesharescount = 0
$healthybackups = 0
$nothealthybackups = 0
$completedbackups = 0
$notcompletedbackups = 0

$servicename = "prtgmon"
$domain = $TenantDomain

if ($HostnameOverwrite) {
    $hostname = $HostnameOverwrite 
} else {
    $hostname = Invoke-Command -ScriptBlock {hostname}
}

$fullname = "$hostname.$servicename-$domain"
$cert = Get-ChildItem -Path Cert:LocalMachine\MY | Where-Object {$_.Subject -like "*$fullname*"}

If (!($cert)) {
    $errortext="No Certificate found. Create Cert and ServicePrincipal first."
		return @"
		<prtg>
		<error>1</error>
		<text>$errortext</text>
		</prtg>
"@
}

Try {
    Connect-AzAccount -ServicePrincipal -Tenant $TenantId -ApplicationId $ApplicationID -CertificateThumbprint $cert.Thumbprint -ErrorAction Stop
} Catch {
    $errortext="$("Connection: " + $_.Exception.Message)"
		return @"
		<prtg>
		<error>1</error>
		<text>$errortext</text>
		</prtg>
"@
}

Try {
    $results = Get-AzFileshareBackupInformation -ErrorAction Stop
} Catch {
    $errortext="$("Function: " + $_.Exception.Message)"
		return @"
		<prtg>
		<error>1</error>
		<text>$errortext</text>
		</prtg>
"@
}

$totalbackedupfileshares = $results.Total_Backedup_Fileshares
$totalnotbackedupfileshares = $results.Total_Not_Backedup_Fileshares

foreach ($result in $results) {
    If ($result.FS_Protected -eq "Healthy") {
        $healthybackups++
    } elseif ($result.FS_Protected) {
        $nothealthybackups++
        $msgtxt += "Unhealthy: "+$FS_BackupPolicy+","
    }
    If ($result.FS_LastBackupStatus -eq "Completed") {
        $completedbackups++
    } elseif ($result.FS_Protected) {
        $notcompletedbackups++
        $msgtxt += "Incomplete: "+$FS_BackupPolicy+","
    }
}

$msgtxt = $msgtxt | Select-Object -Unique

$XML = "<prtg>"
$XML += "<text>$msgtxt</text>"
$XML += "<result><channel>Total BackedUp Fileshares</channel><value>$totalbackedupfileshares</value><unit>Custom</unit></result>"
$XML += "<result><channel>Total Not BackedUp Fileshares</channel><value>$totalnotbackedupfileshares</value><unit>Custom</unit></result>"
$XML += "<result><channel>Total Healthy Backups</channel><value>$healthybackups</value><unit>Custom</unit></result>"
$XML += "<result><channel>Total Completed Backups</channel><value>$completedbackups</value><unit>Custom</unit></result>"
$XML += "<result><channel>Total Unhealthy Backups</channel><value>$nothealthybackups</value><unit>Custom</unit><LimitMode>1</LimitMode><LimitMaxError>0</LimitMaxError></result>"
$XML += "<result><channel>Total Incomplete Backups</channel><value>$notcompletedbackups</value><unit>Custom</unit><LimitMode>1</LimitMode><LimitMaxError>0</LimitMaxError></result>"
$XML += "</prtg>"

Function WriteXmlToScreen ([xml]$xml) 
{
    $StringWriter = New-Object System.IO.StringWriter;
    $XmlWriter = New-Object System.Xml.XmlTextWriter $StringWriter;
    $XmlWriter.Formatting = "indented";
    $xml.WriteTo($XmlWriter);
    $XmlWriter.Flush();
    $StringWriter.Flush();
    Write-Output $StringWriter.ToString();
}
WriteXmlToScreen "$XML"