﻿Param( [string] $dfsSrcSrv, [string] $dfsDstSrv )
$ErrorActionPreference = "Continue"

# -------------------------------------------------------------------

$PRTG_XML_OK = 0                   # OK
$PRTG_XML_WARNING = 1              # Warning
$PRTG_XML_ERROR_SYSTEM = 2         # System Error (e.g. a network/socket error)
$PRTG_XML_ERROR_PROTOCOL = 3       # Protocol Error (e.g. web server returns a 404)
$PRTG_XML_ERROR_CONTENT = 4        # Content Error (e.g. a web page does not contain a required word)

function PRTG-XML-ReturnError {
    Param( [int] $prtg_id,
           [string] $message)

    Write-Host "<error>$prtg_id</error>"
    Write-Host "<text>$message</text>"
    Write-Host "</prtg>"
    exit
}

# -------------------------------------------------------------------

if( -Not ($dfsSrcSrv) ) {
    PRTG-XML-ReturnError -prtg_id $PRTG_XML_ERROR_SYSTEM -message "No DFS source server specified"
}
if( -Not ($dfsDstSrv) ) {
    PRTG-XML-ReturnError -prtg_id $PRTG_XML_ERROR_SYSTEM -message "No DFS destination server specified"
}

$dfsgrps = Get-DfsReplicationGroup

Write-Host "<prtg>"

foreach($dfsgrp in $dfsgrps) {
    try
    {
        $res = (Get-DfsrBacklog -GroupName $($dfsgrp.GroupName) -FolderName * -SourceComputerName $($dfsSrcSrv) -DestinationComputerName $($dfsDstSrv) -Verbose 4>&1 -ErrorAction Stop).Message.Split(':')[2]
        $res = $res -replace '\s',''  
        Write-Host "<result>"
        Write-Host "    <channel>$($dfsgrp.GroupName -replace '.*\\')</channel>"
        Write-Host "    <value>$(if (!$res) { '0' } else { $res })</value>"
        Write-Host "    <unit>Count</unit>"
        # Standard Limits ( only valid on sensor creation, not after it!):
        Write-Host "<LimitMode>1</LimitMode>"
        Write-Host "<LimitMaxError>250</LimitMaxError>"
        Write-Host "<LimitMaxWarning>50</LimitMaxWarning>"
        #End Standard Limit
        Write-Host "</result>"

    }
    catch
    {
        PRTG-XML-ReturnError -prtg_id $PRTG_XML_ERROR_SYSTEM -message $_.Exception.Message
        $error.Clear()
    }
}
Write-Host "</prtg>"