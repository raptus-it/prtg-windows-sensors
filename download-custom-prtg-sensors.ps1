function DownloadGitHubRepository 
{ 
    param( 
       [Parameter(Mandatory=$True)] 
       [string] $Name, 
         
       [Parameter(Mandatory=$False)] 
       [string] $Author = "raptus-it", 
         
       [Parameter(Mandatory=$False)] 
       [string] $Branch = "master", 
         
       [Parameter(Mandatory=$False)] 
       [string] $Location = "C:\Program Files (x86)\PRTG Network Monitor"
    ) 
     
    # Force to create a zip file 
    $ZipFile = "$Location\$Name.zip"
    New-Item $ZipFile -ItemType File -Force
 
    #$RepositoryZipUrl = "https://bitbucket.org/raptus-it/raptus-std-windows-server/get/master.zip"
    $RepositoryZipUrl = "https://bitbucket.org/$Author/$Name/get/$Branch.zip" 
    # download the zip 
    Write-Host "Starting downloading the GitHub Repository"
    Invoke-RestMethod -Uri $RepositoryZipUrl -OutFile $ZipFile
    Write-Host "Download finished"
 
    #Extract Zip File
    Write-Host "Starting unzipping the GitHub Repository locally"
    Expand-Archive -Path $ZipFile -DestinationPath $location -Force
    Write-Host "Unzip finished, Repo Downloaded to: $Location"

    # remove the zip file
    Get-ChildItem -Directory -Path $Location -Filter "*$Name*" | Rename-Item -NewName $Name
    Remove-Item -Path $ZipFile -Force
}

# Cleanup PRTG Folder
$prtgpath = "C:\Program Files (x86)\PRTG Network Monitor"
Try {
    If (Test-Path "$prtgpath\prtg-windows-sensors") {
        Remove-Item "$prtgpath\prtg-windows-sensors" -Recurse -Force
    }
    If (Test-Path "$prtgpath\Custom Sensors") {
        Remove-Item "$prtgpath\Custom Sensors" -Recurse -Force
    }
} Catch { Write-Host "$_.Exception.Message" }

# Download Bitbucket Repo
Try {
    DownloadGitHubRepository -Name "prtg-windows-sensors"
    Rename-Item "$prtgpath\prtg-windows-sensors" "$prtgpath\Custom Sensors"
} Catch { Write-Host "$_.Exception.Message" }

# Install necessary PS Modules
Try {
    Write-Host "Installing some PSModules, this can take a while"
    Register-PSRepository -Default -ErrorAction SilentlyContinue
    Install-Module AzureAD -Scope AllUsers -Force -ErrorAction Stop
    Install-Module Az -Scope AllUsers -Force -ErrorAction Stop
} Catch { Write-Host "$_.Exception.Message" }

#eof