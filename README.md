# README #

### What is this repository for? ###
* This Repository contains all selfmade PRTG Sensors for Windows Systems.

### General
* Sensor preferrably works in 64Bit Powershell
* PRTG is running 32Bit Powershell

### OS Support
* Support for Windows 2012R2 and higher

### Sensor installation
* Install all PRTG Sensor by running the following command:

````
Set-ExecutionPolicy -ExecutionPolicy Unrestricted
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
(new-object net.webclient).DownloadFile("https://bitbucket.org/raptus-it/prtg-windows-sensors/raw/master/download-custom-prtg-sensors.ps1","download-custom-prtg-sensors.ps1"); .\download-custom-prtg-sensors.ps1
````
### Azure Sensors Connection
Since Security Defaults, the Azure Legacy Auth based scripts do not work anymore. There is a new script to create a service principal for our Azure Sensors.
It will be automatically downloaded with the Installation-Command.

````powershell
cd "${Env:ProgramFiles(x86)}\PRTG Network Monitor\Custom Sensors"
.\create-service-principal-for-prtg.ps1 -domain "<cust.tld>"
````

### PRTG Sensor
* The sensors should now be available in PRTG if you add a new sensor on the probe device

### Troubleshooting
* Try changing the sensors security context to "Use Windows credentials of parent device"
* Try running the script in a 64Bit PowerShell with no parameters
* Check the eventlog for more details
