# creates a selfsigned certificate and a service principal in azure
# this is a slightly modified script to create a PRTG Principal only
# 
# PARAMS:
# -domain: customer azuread domain
#
# Register-PSRepository -Default
# Install-Module Az; Import-Module Az
# Install-Module AzureAD; Import-Module AzureAD

#PARAMS
Param (
    [Parameter(Mandatory=$true)]
    [ValidateNotNullOrEmpty()] 
    [string]$domain
)

#FUNCTIONS
function Write-Log {

    [CmdletBinding()] 
    Param (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()] 
        [string]$Message,

        [Parameter(Mandatory=$false)]
        [ValidateSet("Information","Warning","Error")]
        [string]$LogLevel="Information",

        [Parameter(Mandatory=$false)]
        [int]$EventID=27002,

        [Parameter(Mandatory=$false)]
        [string]$Source = (Get-Item $MyInvocation.ScriptName).Name,

        [Parameter(Mandatory=$false)]
        [switch]$Console
    )
    Begin {
        if ($Console -and $LogLevel -eq "Information") {
            Write-Host -ForegroundColor green $Message
        } elseif ($Console -and $LogLevel -eq "Warning") {
            Write-Host -ForegroundColor yellow $Message
        } elseif ($Console -and $LogLevel -eq "Error") {
            Write-Host -ForegroundColor red $Message
        }         
        if ((Get-Host).Version.Major -gt 5) {
            exit 0    
        }
    } Process {
        $VerbosePreference = 'Continue'
        $startmsg = Get-EventLog -list | Where-Object {$_.logdisplayname -eq "Raptus"}
        if (-Not $startmsg) {
            New-EventLog -LogName Raptus -Source "Raptus" -ErrorAction SilentlyContinue
            Write-EventLog -LogName Raptus -Source "Raptus" -EntryType Information -EventId 27001 -Message "Raptus Log Start"
        }
        $checksource = [System.Diagnostics.EventLog]::SourceExists($Source);
        if (-Not $checksource) {
            New-EventLog -LogName Raptus -Source $Source
        }
    } End {
        Write-EventLog -LogName Raptus -Message $Message -EntryType $LogLevel -EventId $EventID -Source $Source
    }
}

function Generate-RandomPassword {
    function Get-RandomCharacters($length, $characters) {
        $random = 1..$length | ForEach-Object { Get-Random -Maximum $characters.length }
        $private:ofs=""
        return [String]$characters[$random]
    }
        
    function Scramble-String([string]$inputString){     
        $characterArray = $inputString.ToCharArray()   
        $scrambledStringArray = $characterArray | Get-Random -Count $characterArray.Length     
        $outputString = -join $scrambledStringArray
        return $outputString 
    }
    $password = Get-RandomCharacters -length 8 -characters 'abcdefghiklmnoprstuvwxyz'
    $password += Get-RandomCharacters -length 8 -characters 'ABCDEFGHKLMNPRSTUVWXYZ'
    $password += Get-RandomCharacters -length 2 -characters '123456789'
    $password += Get-RandomCharacters -length 2 -characters '£_-:;$][!?)(/&%*@+'

    return Scramble-String $password
}

#Generate SelfSigned Certificate
$servicename = "prtgmon"
$currentDate = Get-Date
$endDate = $currentDate.AddYears(10)
$notAfter = $endDate.AddYears(10)
$password = Generate-RandomPassword
$path = "c:\raptus\selfsigned"
$hostname = Invoke-Command -ScriptBlock {hostname}
$fullname = "$hostname.$servicename-$domain"

If  (!(Test-Path $path)) {
    New-Item -ItemType Directory -Force -Path $path
}

Try {
    $thumb = (New-SelfSignedCertificate -CertStoreLocation cert:\localmachine\my -DnsName $fullname -KeyExportPolicy Exportable -Provider "Microsoft Enhanced RSA and AES Cryptographic Provider" -NotAfter $notAfter -ErrorAction Stop).Thumbprint
    $password | Out-File -FilePath $path\$fullname.txt
    $password = ConvertTo-SecureString -String $password -Force -AsPlainText
    Export-PfxCertificate -cert "cert:\localmachine\my\$thumb" -FilePath $path\$fullname.pfx -Password $password -ErrorAction Stop 
    Write-Log -Message "SelfSigned Certificate Generated" -Console
} Catch {
    Write-Log -Message $_.Exception.Message -LogLevel Error -Console
}

#Login to Azure AD PowerShell With Admin Account
Connect-AzAccount

#Load the certificate
Try {
    $cert = New-Object System.Security.Cryptography.X509Certificates.X509Certificate("$path\$fullname.pfx", $password) -ErrorAction Stop
    $keyValue = [System.Convert]::ToBase64String($cert.GetRawCertData())
    Write-Log -Message "Certificate Prepared" -Console
} Catch {
    Write-Log -Message $_.Exception.Message -LogLevel Error -Console
}

#Create the Azure Active Directory Application
Try {
    $application = New-AzADApplication -DisplayName $servicename -IdentifierUris "https://$servicename.$domain"
    New-AzADAppCredential -ApplicationId $application.AppId -CertValue $keyValue -StartDate $currentDate -EndDate $endDate
    Write-Log -Message "Azure AD App Registered" -Console
} Catch {
    Write-Log -Message $_.Exception.Message -LogLevel Error -Console
}

#Create the Service Principal and connect it to the Application
Try {
    Write-Log -Message "Creating Service Principal" -Console
    $sp=New-AzADServicePrincipal -AppId $application.AppId
    
    #Make the Service Principal Subscription Reader
    $Subscription = Get-AzSubscription
    $SubscriptionId = $Subscription.Id
    Write-Log -Message "Adding Roles to the new Principal" -Console
    New-AzRoleAssignment -ApplicationId $sp.AppId  -Scope "/subscriptions/$SubscriptionId" -RoleDefinitionName "Reader"
    New-AzRoleAssignment -ApplicationId $sp.AppId  -Scope "/subscriptions/$SubscriptionId" -RoleDefinitionName "Backup Reader"
} Catch {
    Write-Log -Message $_.Exception.Message -LogLevel Error -Console
}