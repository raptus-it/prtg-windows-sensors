$fwProf = @("Domain","Private","Public")
[System.Collections.ArrayList]$theTruth = @()
$winv = (Get-WmiObject -class Win32_OperatingSystem).Caption
if ($winv -like "*2008*" )
{
    foreach ($element in $fwProf)
    {
        $stat = netsh advfirewall show $element state | select -Index 3
        if(($stat -match "ON") -Or ($stat -match "EIN"))
        {
            [void]$theTruth.Add("True")
        } elseif (($stat -match "OFF") -Or ($stat -match "AUS"))
        {
            [void]$theTruth.Add("False")
        }
    }
    if ($theTruth -notcontains "False")
    {
        Write-Host "0:OK"
        exit 0
    }
}

elseif (($winv -like "*2012*") -Or ($winv -like "*2016*"))
{
    $stat = get-netfirewallprofile | select Enabled
    if($stat.Enabled -notcontains "False")
    {
        Write-Host "0:OK"
        exit 0
    }
}
Write-Host "2:Windows Firewall Check needed"
exit 2