Param( [string] $processName, [string] $userName )
$ErrorActionPreference = "Continue"

if (-Not ($processName)) 
{
    Write-Host "2:Process name not specified"
    exit 2
}

if (-Not ($userName)) 
{
    Write-Host "2:User name not specified"
    exit 2
}

$dbProcCountForUser = (Get-Process -IncludeUserName | Where-Object { $_.ProcessName -eq $processName -and $_.UserName -eq $userName } | measure).Count
if ($dbProcCountForUser -eq 3)
{
    Write-Host "0:OK"
    exit 0
}

Write-Host "2:$processName for user $userName is not running"
exit 2
