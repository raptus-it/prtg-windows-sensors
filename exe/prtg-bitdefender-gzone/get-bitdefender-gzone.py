#!/opt/local/bin/python
# -*- coding: utf-8 -*-

import os
import io
import sys
import base64
import hashlib
import argparse
import time
import datetime
import json
import re
import unicodedata
import urllib
import requests
import simplejson

from pyjsonrpc import rpcrequest
from pyjsonrpc import rpcjson
from collections import Counter

from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

PRTG_OK = 0                                 # OK
PRTG_WARNING = 1                            # Warning
PRTG_ERROR_SYSTEM = 2                       # System Error (e.g. a network/socket error)
PRTG_ERROR_PROTOCOL = 3                     # Protocol Error (e.g. web server returns a 404)
PRTG_ERROR_CONTENT = 4                      # Content Error (e.g. a web page does not contain a required word)

PRTG_API_COMMENT_URL = "https://monitoring.raptus.com/api/setobjectproperty.htm"
PRTG_API_CRED_FILE = "auth/prtg-api-auth.json"

GZONE_API_URL = "https://cloudgz.gravityzone.bitdefender.com/api/v1.0/jsonrpc/"
GZONE_API_CRED_FILE = "auth/gzone-api-auth.json"
GZONE_API_DELAY = 0.01                      # seconds: each API call is delayed by this delay
GZONE_API_PER_PAGE = 30                     # how many results per page for getEndpointsList
GZONE_API_DATE_FORMAT = "%Y-%m-%dT%H:%M:%S" # format for strptime() for data returned by API

LAST_REPORT_STATE_PREFIX = "states/lc"      # filename prefix of last reported states

CACHE_FOLDER = "cache"                      # no trailing slash 
CACHE_MAX_AGE = 60*30                       # seconds: invalidate cache files after this period
ENCODING = "utf-8"                          # encoding 

NL = " \r\n"
SEPARATOR = " | "
NOW = datetime.datetime.now()
ONE_DAY = 60*60*24

ALERT_GROUP_DELETED = u"Excluded"           # if this group is not empty, a warning is raised
ALERT_THRESHOLD_LASTSEEN = ONE_DAY*90       # seconds: delay for "endpointOutdated"
ALERT_THRESHOLD_LASTUPDATE = ONE_DAY*3      # seconds: delay for "signatureOutdated" (custom!)

# Unfortunatley the Bitdefender API has not been told, that ATC is only available on certain
# platforms. And there is no documentation on which platform it is available. 
# This is why this list has been compiled with trial & error and lots of frustration.

ATC_UNSUPPORTED_OSES = [                    # List of OS ID strings on which ATC is not available
                            "Linux",
                            "macOS",
                            "Microsoft Windows XP",
                            "Microsoft Windows Server 2003",
                            "Microsoft Windows Server 2003 R2"
                       ]

#####################################################################

def json_load(filename):
    if os.path.isfile(filename):
        file = io.open(filename, "r", encoding=ENCODING)
        data = json.load(file)
        file.close()
        return data
    return None

def json_write(filename, data):
    cf = io.open(filename, "w", encoding=ENCODING)
    cf.write(unicode(json.dumps(data, ensure_ascii=False)))
    cf.close()

def prtg_comment_overwrite(comment, sensorID):
    prtg_auth = json_load(PRTG_API_CRED_FILE)
    prtg_get_params = {
                        "username" : prtg_auth["prtg_username"], 
                        "passhash" : prtg_auth["prtg_passhash"],
                        "id": sensorID,
                        "name": "comments",
                        "value" : comment
                      }
    ret = requests.get(PRTG_API_COMMENT_URL, params=urllib.urlencode(prtg_get_params, ENCODING))
    return ret.status_code == 200 and ("OK" in ret.text)

def prtg_return(eid, error, value=-1):
    """ Returns a NOT OK value to PRTG
    """
    print str(value) + ":" + error
    sys.exit(eid)

def cache_file(key):
    """ Returns the cache file for a given cache key
        Yeah i know a bit dirty to access a global var like this, but never mind ;)
    """
    return CACHE_FOLDER + "/" + customer_filename + "_" + key + ".json"

def cache_get(key):
    """ Retrieve JSON data from cache file
    """
    cfname = cache_file(key)
    if os.path.isfile(cfname):
        tdelta =  NOW - datetime.datetime.fromtimestamp(os.path.getmtime(cfname))
        if tdelta.total_seconds() > CACHE_MAX_AGE:
            os.remove(cfname)

    return json_load(cfname)

def cache_update(key, data):
    """ Store JSON data into cache file
    """
    cfname = cache_file(key)
    json_write(cfname, data)

def api_call(apiId, method, *args, **kwargs):
    """ Call API, do cacheing, delay and error handling
    """

    # need to create a dict (not a string) to be able to remove generated ID
    request_dict = rpcrequest.create_request_dict(method, *args, **kwargs)

    # create cache key
    request_noid = request_dict.copy()
    request_noid.pop("id")
    cache_key = method + "_" + hashlib.md5(rpcjson.dumps(request_noid)).hexdigest()

    # first try to get JSON from cache
    jsonResult = cache_get(cache_key)
    if not jsonResult:
        # well nothing in cache, let's call the API
        time.sleep(GZONE_API_DELAY)
        result = requests.post(GZONE_API_URL + apiId, rpcjson.dumps(request_dict), verify=False, headers=http_headers)
        jsonResult = simplejson.loads(result.content)
        cache_update(cache_key, jsonResult)

    if jsonResult:
        if "error" in jsonResult:
            if jsonResult["error"]["data"]["details"] == "Monthly licensing is not supported for the given company.":
                # Special handling, since some customers are not billed monthly (method: getMonthlyUsage)
                # Monthly billing is default and preferred
                if not cli_args.anomonthly:
                    prtg_return(PRTG_ERROR_SYSTEM, "Customer is not billed monthly. If this is correct and monitoring "
                                                   "is included override with parameter 'nomonthly'")
            else:
                # Print a maximum of infos if API error (hope PRTG can display them all)
                prtg_return(PRTG_ERROR_SYSTEM, 
                            "API ID: " + apiId + ", API METHOD: " + method + 
                            ", API ERROR: " + str(jsonResult["error"]["code"]) + 
                            ": " + jsonResult["error"]["message"] + 
                            " - " + jsonResult["error"]["data"]["details"] +
                            " REQUEST: " + rpcjson.dumps(request_dict))

        # everything is find, return only the result
        if "result" in jsonResult:
            return jsonResult["result"]

    return None

def api_findCompany(companyName):
    """ Find the customer. Error if multiple found or customer is suspended.
    """
    retval = api_call("companies", "findCompaniesByName", nameFilter=companyName)
    if not retval:
        prtg_return(PRTG_ERROR_CONTENT, "Company <" + customer + "> not found")

    if len(retval) > 1:
        prtg_return(PRTG_ERROR_CONTENT, "Company <" + customer + "> not clearly identified.")

    if retval[0]["isSuspended"]:
        prtg_return(PRTG_ERROR_CONTENT, "Company <" + customer + "> is SUSPENDED")

    return retval[0]

def api_getMonthlyUsage(companyId, targetMonth=None):
    """ Return usage of current month 
    """
    if not targetMonth:
        targetMonth = datetime.datetime.now().strftime("%m/%Y")
    return api_call("licensing", "getMonthlyUsage", companyId=companyId, targetMonth=targetMonth)

def api_getCustomGroupsList(parentId):
    """ Get list of groups for a certain ID (customer or other group)
    """
    return api_call("network", "getCustomGroupsList", parentId=parentId)

def api_getEndpointsList(parentId, page=1, perPage=GZONE_API_PER_PAGE):
    """ Get list of endpoints in a group, returns one page of results
    """
    return api_call("network", "getEndpointsList", parentId=parentId, page=page, perPage=perPage)

def api_getManagedEndpointDetails(endpointId):
    """ Get all available details for an endpoint
    """
    return api_call("network", "getManagedEndpointDetails", endpointId=endpointId)

def buildCustomGroupsList(companyId, groupList = None):
    """ Recurisvely build list of custom groups and subgroups
    """
    if not groupList:
        groupList = []

    retval = api_getCustomGroupsList(companyId)
    if retval:
        for item in retval:
            if item:
                groupList.append(item)
                buildCustomGroupsList(item["id"], groupList)
    return groupList

def buildEndpointsList(groupId, groupName, endpointList, unmanagedCount, page=1):
    """ Recursivley build list of endpoints and enrich data
    """
    retval = api_getEndpointsList(groupId, page=page)
    if retval and retval["items"]:
        for item in retval["items"]:
            if item["isManaged"]:
                epd = api_getManagedEndpointDetails(item["id"])

                if epd["lastSeen"]:
                    lastSeen = datetime.datetime.strptime(epd["lastSeen"], GZONE_API_DATE_FORMAT)
                else:
                    lastSeen = NOW

                # Workaround for known bug: lastSeen is in the future
                if lastSeen > NOW:
                    lastSeen = NOW

                # If the lastUpdate is empty there must be some kind of error which needs investigation
                # Set the date way too old to let the sensor go into down state
                lastUpdate = datetime.datetime(1980, 01, 01)
                if epd["agent"]["lastUpdate"]:
                    lastUpdate = datetime.datetime.strptime(epd["agent"]["lastUpdate"], GZONE_API_DATE_FORMAT)

                # Alert if endpoint has not been seen in a long time
                endpointOutdated = (NOW - lastSeen).total_seconds() > ALERT_THRESHOLD_LASTSEEN                

                # Want to avoid too much alerts for outdated signatures
                # So alert if endpoint's signatures are outdated since a long time
                signatureReallyOutdated = (lastSeen - lastUpdate).total_seconds() > ALERT_THRESHOLD_LASTUPDATE

                # Flatten data and pick needed values
                epdi = { 
                            "id":                       item["id"] ,
                            "name":                     epd["name"],
                            "machineType":              epd["machineType"],
                            "os":                       epd["operatingSystem"],
                            "state":                    epd["state"],
                            "lastSeen":                 epd["lastSeen"],
                            "lastUpdate":               epd["agent"]["lastUpdate"],
                            "licensed":                 epd["agent"]["licensed"],
                            "endpointOutdated":         endpointOutdated,
                            "prodOutdated":             epd["agent"]["productOutdated"],
                            "prodUpdateDisabled":       epd["agent"]["productUpdateDisabled"],
                            "sigOutdated":              epd["agent"]["signatureOutdated"],
                            "sigReallyOutdated":        signatureReallyOutdated,
                            "sigUpdateDisabled":        epd["agent"]["signatureUpdateDisabled"],
                            "group":                    epd["group"]["name"],
                            "detection":                epd["malwareStatus"]["detection"],
                            "infected":                 epd["malwareStatus"]["infected"],
                            "atc":                      epd["modules"]["advancedThreatControl"],
                            "antimalware":              epd["modules"]["antimalware"]
                       }

                # Harmonize OS version info for ATC check
#                print epdi["os"]
                if epdi["os"].startswith("Mac OS X"):
                    epdi["os"] = "macOS"
                if epdi["os"].startswith("macOS"):
                    epdi["os"] = "macOS"
                if epdi["os"].startswith("Linux"):
                    epdi["os"] = "Linux"

                endpointList.append(epdi)
            else:
                # Unmanaged endpoints are worthless
                unmanagedCount += 1

        # respect pagination of API
        pages = abs(retval["total"] / GZONE_API_PER_PAGE)
        if page == 1 and pages > 1:
            pge = 1
            while pge < pages:
                pge += 1
                buildEndpointsList(groupId, groupName, endpointList, unmanagedCount, page=pge)

#####################################################################
# Main
#####################################################################

# parse CLI arguments
parser = argparse.ArgumentParser()
parser.add_argument("-c", "--customer",  dest="acustomer",  required=True,  type=str, 
                    help="Customer identification string")

parser.add_argument("-l", "--lics",  dest="alics",  required=True,  type=int, 
                    help="Paid license count")

parser.add_argument("-d", "--debug",     dest="adebug",     required=False, type=str, 
                    help="Specify SensorID to print debug info to PRTG comments. Overwrites existing comments")

parser.add_argument("-n", "--nomonthly", dest="anomonthly", required=False, default=False, action="store_true", 
                    help="Override if customer is not billed monthly, but still monitored")

parser.add_argument("-omd", "--nomoderror", dest="anomoderror", required=False, default=False, action="store_true",
                    help="Override if disabled modules should only generate warnings instead of errors")
cli_args = parser.parse_args()

# ensure proper encoding for customer name
customer = cli_args.acustomer.decode(sys.getfilesystemencoding()) 

# normalize customername to use it as filename
customer_filename = unicodedata.normalize('NFKD', customer).encode('ascii', 'ignore')
customer_filename = unicode(re.sub('[^\w\s-]', '', customer_filename).strip().lower())
customer_filename = unicode(re.sub('[-\s]+', '-', customer_filename))

# prepare API authentication
gzone_auth = json_load(GZONE_API_CRED_FILE)
encodedUserPassSequence = base64.b64encode(gzone_auth["gzone_api_key"] + ":")
authorizationHeader = "Basic " + encodedUserPassSequence
http_headers = { "Content-Type": "application/json", "Authorization": authorizationHeader }

# initialize variables used for PRTG result
countEndpoints = -1
countManaged = -1
countUnmanaged = -1
countMT0_Other = -1
countMT2_Computer = -1
countMT3_VirtualMachine = -1
countDuplicates = -1
countMonthlyEndpoints = -1
countMonthlyExchange = -1
countStateUnknown = -1
countStateOnline = -1
countStateOffline = -1
countStateSuspended = -1
countLicensedPending = -1
countLicensedActive = -1
countLicensedExpired = -1
countLicensedNoLic = -1
countProdOutdated = -1
countSigOutdated = -1
countProdUpdateDisabled = -1
countSigUpdateDisabled = -1
countDetections = -1
countInfections = -1
countAntimalwares = -1
countAdvThreatControls = -1

# check if cache folder is there, assume it's writeable
if not os.path.isdir(CACHE_FOLDER):
    prtg_return(PRTG_ERROR_SYSTEM, "Cache folder <" + CACHE_FOLDER + "> not found")

# retrieve customer dict
co = api_findCompany(customer)

# retrieve monthly usage
mu = api_getMonthlyUsage(co["id"])
if mu:
    countMonthlyEndpoints = mu["endpointMonthlyUsage"]
    countMonthlyExchange = mu["exchangeMonthlyUsage"]

# build groups list
gl = buildCustomGroupsList(co["id"])

# build list of endpoints
countUnmanaged = 0
endpointList = []
for group in gl:
    buildEndpointsList(group["id"], group["name"], endpointList, countUnmanaged)

# calculate and prepare data, fill into PRTG results
if len(endpointList) > 0:
    # Sum up values per dict group
    cntrNames               = Counter(item["name"] for item in endpointList)
    cntrGroups              = Counter(item["group"] for item in endpointList)
    cntrMachineTypes        = Counter(item["machineType"] for item in endpointList)
    cntrStates              = Counter(item["state"] for item in endpointList)
    cntrLicensed            = Counter(item["licensed"] for item in endpointList)
    cntrEndpointOutdated    = Counter(item["endpointOutdated"] for item in endpointList)
    cntrProdOutdated        = Counter(item["prodOutdated"] for item in endpointList)
    cntrSigOutdated         = Counter(item["sigReallyOutdated"] for item in endpointList) # only consider our value
    cntrProdUpdateDisabled  = Counter(item["prodUpdateDisabled"] for item in endpointList)
    cntrSigUpdateOutdated   = Counter(item["sigUpdateDisabled"] for item in endpointList)
    cntrDetections          = Counter(item["detection"] for item in endpointList)
    cntrInfections          = Counter(item["infected"] for item in endpointList)
    cntrAntimalwares        = Counter(item["antimalware"] for item in endpointList)
    cntrAdvThreatControls   = Counter(item["atc"] for item in endpointList if item["os"] not in ATC_UNSUPPORTED_OSES) # ATC only for certain OSes

    countEndpoints = len(endpointList)
    countManaged = countEndpoints-countUnmanaged
    countDeleted = cntrGroups[ALERT_GROUP_DELETED]

    # Type of the machine: 1 - computer, 2 - virtual machine, 3 - EC2 Instance, 0 - Other
    countMT0_Other = cntrMachineTypes[0] 
    countMT1_Computer = cntrMachineTypes[1]
    countMT2_VirtualMachine = cntrMachineTypes[2]

    # Count how many endpoints exist more than once, depending on hostname
    countDuplicates = sum(item > 1 for item in cntrNames.values())

    # State - the power state of the machine: 1 - online, 2 - offline, 3 - suspended; 0 - unknown
    countStateUnknown = cntrStates[0] 
    countStateOnline = cntrStates[1]
    countStateOffline = cntrStates[2]
    countStateSuspended = cntrStates[3]

    # Status of the license: 0 - pending authentication, 1 - active license, 2 - expired license, 6 - there is no license or not applicable
    countLicensedPending = cntrLicensed[0]
    countLicensedActive = cntrLicensed[1]
    countLicensedExpired = cntrLicensed[2]
    countLicensedNoLic = cntrLicensed[6]

    countEndpointOutdated = cntrEndpointOutdated[True]
    countProdOutdated = cntrProdOutdated[True]
    countSigOutdated = cntrSigOutdated[True]
    countProdUpdateDisabled = cntrProdUpdateDisabled[True]
    countSigUpdateDisabled = cntrSigUpdateOutdated[True]
    countDetections = cntrDetections[True]
    countInfections = cntrInfections[True]
    countAntimalwares = cntrAntimalwares[False]
    countAdvThreatControls = cntrAdvThreatControls[False]
         
#####################################################################
# PRTG 
#####################################################################

return_msg = ""
return_val = PRTG_OK
debug_info = ""

### HARD ERRORS

if countInfections > 0:
    return_msg += "INFECTED endpoints: " + str(countInfections) + SEPARATOR
    debug_info += "INFECTED endpoints: " + NL + ", ".join( item["name"] for item in endpointList if item["infected"] == True) + NL*2
    return_val = PRTG_ERROR_CONTENT if return_val == PRTG_OK else return_val

if countDetections > 0:
    return_msg += "DETECTED malware: " + str(countDetections) + SEPARATOR
    debug_info += "DETECTED malware: " + NL + ", ".join( item["name"] for item in endpointList if item["detection"] == True) + NL*2
    return_val = PRTG_ERROR_CONTENT if return_val == PRTG_OK else return_val

### PROTOCOL ERRORS

if countManaged <= 0:
    return_msg += "No managed endpoints found" + SEPARATOR
    return_val = PRTG_ERROR_PROTOCOL if return_val == PRTG_OK else return_val

if countProdUpdateDisabled > 0:
    label = "Agent update disabled: "
    return_msg += label + str(countProdUpdateDisabled) + SEPARATOR
    debug_info += label + NL + ", ".join( item["name"] for item in endpointList if item["prodUpdateDisabled"] == True) + NL*2
    return_val = PRTG_ERROR_PROTOCOL if return_val == PRTG_OK else return_val

if countSigUpdateDisabled > 0:
    label = "Signature update disabled: "
    return_msg += label + str(countSigUpdateDisabled) + SEPARATOR
    debug_info += label + NL + NL + ", ".join( item["name"] for item in endpointList if item["sigUpdateDisabled"] == True) + NL*2
    return_val = PRTG_ERROR_PROTOCOL if return_val == PRTG_OK else return_val

if countDeleted > 0:
    label = "Deleted endpoints: "
    return_msg += label + str(countDeleted)+ SEPARATOR
    debug_info += label + NL + ", ".join( item["name"] for item in endpointList if item["group"] == ALERT_GROUP_DELETED) + NL*2
    return_val = PRTG_ERROR_PROTOCOL if return_val == PRTG_OK else return_val

if countDuplicates > 0:
    return_msg += "Duplicate endpoints: " + str(countDuplicates) + SEPARATOR
    debug_info += "Duplicate endpoints: " + NL + ", ".join( item["name"] for item in endpointList if cntrNames[item["name"]] > 1) + NL*2
    return_val = PRTG_ERROR_PROTOCOL if return_val == PRTG_OK else return_val

#if countStateUnknown > 0:
#    return_msg += "Unknown endpoints: " + str(countStateUnknown) + SEPARATOR
#    debug_info += "Unknown endpoints: " + NL + ", ".join( item["name"] for item in endpointList if item["state"] > 0) + NL*2
#    return_val = PRTG_ERROR_PROTOCOL if return_val == PRTG_OK else return_val

if countUnmanaged > 0:
    return_msg += "Unmanaged endpoints: " + str(countUnmanaged) + SEPARATOR
    return_val = PRTG_ERROR_PROTOCOL if return_val == PRTG_OK else return_val

if countAntimalwares > 0:
    return_msg += "AntiMalware inactive: " + str(countAntimalwares) + " EPs" + SEPARATOR
    debug_info += "AntiMalware inactive: " + NL + ", ".join( item["name"] for item in endpointList if item["antimalware"] == False) + NL*2
    if cli_args.anomoderror:
        return_val = PRTG_WARNING if return_val == PRTG_OK else return_val
    else:
        return_val = PRTG_ERROR_PROTOCOL if return_val == PRTG_OK else return_val

if countAdvThreatControls > 0:
    return_msg += "ATC inactive: " + str(countAdvThreatControls) + " EPs" + SEPARATOR
    debug_info += "ATC inactive: " + NL + ", ".join( item["name"] for item in endpointList if item["atc"] == False) + NL*2
    if cli_args.anomoderror:
        return_val = PRTG_WARNING if return_val == PRTG_OK else return_val
    else:
        return_val = PRTG_ERROR_PROTOCOL if return_val == PRTG_OK else return_val

#if countLicensedPending > 0:
#    return_msg += "Pending licenses: " + str(countLicensedPending) + SEPARATOR
#    debug_info += "Pending licenses: " + NL + ", ".join( item["name"] for item in endpointList if item["licensed"] == 0) + NL*2
#    return_val = PRTG_ERROR_PROTOCOL if return_val == PRTG_OK else return_val

if cli_args.anomonthly and countLicensedExpired > 0:
    return_msg += "Bought licenses expired: " + str(countLicensedExpired) + SEPARATOR
    debug_info += "Bought licenses expired: " + NL + ", ".join( item["name"] for item in endpointList if item["licensed"] == 2) + NL*2
    return_val = PRTG_ERROR_PROTOCOL if return_val == PRTG_OK else return_val

#if countLicensedNoLic > 0:
#    return_msg += "Unlicensed: " + str(countLicensedNoLic) + SEPARATOR
#    debug_info += "Unlicensed: " + NL + ", ".join( item["name"] for item in endpointList if item["licensed"] == 6) + NL*2
#    return_val = PRTG_ERROR_PROTOCOL if return_val == PRTG_OK else return_val

#if countLicensedActive > countManaged:
#    return_msg += "Licenses do not match managed (Lic: " + str(countLicensedActive) + ", EPs: " + str(countManaged) + ")"  + SEPARATOR
#    return_val = PRTG_ERROR_PROTOCOL if return_val == PRTG_OK else return_val

if countMonthlyEndpoints > cli_args.alics:
    return_msg += "Using too much licenses (Reported: " + str(countMonthlyEndpoints) + ", Billed: " + str(cli_args.alics) + ")" + SEPARATOR
    return_val = PRTG_ERROR_PROTOCOL if return_val == PRTG_OK else return_val

if countMonthlyExchange > 0:
    return_msg += "Paying for unused Exchange: " + str(countLicensedNoLic) + SEPARATOR
    return_val = PRTG_ERROR_PROTOCOL if return_val == PRTG_OK else return_val

### WARNINGS

#if countLicensedActive < countManaged:
#    return_msg += "Not enough licenses (Lic: " + str(countLicensedActive) + ", EPs: " + str(countManaged) + ")" + SEPARATOR
#    return_val = PRTG_WARNING if return_val == PRTG_OK else return_val

if countProdOutdated > 0:
    return_msg += "Outdated agent updates: " + str(countProdOutdated) + SEPARATOR
    debug_info += "Outdated agent updates: " + NL + ", ".join( item["name"] for item in endpointList if item["prodOutdated"] == True) + NL*2
    return_val = PRTG_WARNING if return_val == PRTG_OK else return_val

if countSigOutdated > 0:
    label = "Outdated >" + str(ALERT_THRESHOLD_LASTUPDATE/ONE_DAY) + "d signatures: "
    return_msg += label + str(countSigOutdated) + SEPARATOR
    debug_info += label + NL + ", ".join( item["name"] for item in endpointList if item["sigReallyOutdated"] == True) + NL*2
    return_val = PRTG_WARNING if return_val == PRTG_OK else return_val

if countStateSuspended > 0:
    return_msg += "Suspended endpoints: " + str(countStateSuspended) + SEPARATOR
    debug_info += "Suspended endpoints: " + NL + ", ".join( item["name"] for item in endpointList if item["state"] == 3) + NL*2
    return_val = PRTG_WARNING if return_val == PRTG_OK else return_val

if countEndpointOutdated > 0:
    label = "Unseen >" + str(ALERT_THRESHOLD_LASTSEEN/ONE_DAY) + "d EPs: "
    return_msg += label + str(countEndpointOutdated) + SEPARATOR
    debug_info += label + NL + ", ".join( item["name"] for item in endpointList if item["endpointOutdated"] == True) + NL*2
    return_val = PRTG_WARNING if return_val == PRTG_OK else return_val

if return_msg.endswith(SEPARATOR):
    return_msg = return_msg.strip()[:-2]

if return_val == PRTG_OK:
    return_msg = "OK"

if len(debug_info) > 0 and cli_args.adebug:
    if not prtg_comment_overwrite(debug_info.strip(), cli_args.adebug):
        return_msg += "Failed to write debug info to comments" + SEPARATOR
        return_val = PRTG_WARNING if return_val == PRTG_OK else return_val

prtg_return(return_val, return_msg, value=countManaged)

# eof
